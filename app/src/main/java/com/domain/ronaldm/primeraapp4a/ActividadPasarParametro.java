package com.domain.ronaldm.primeraapp4a;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import static android.icu.lang.UCharacter.GraphemeClusterBreak.V;

public class ActividadPasarParametro extends AppCompatActivity {

    EditText cajaDatos;
    Button buttonEnviar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_actividad_pasar_parametro);
        cajaDatos = (EditText) findViewById(R.id.txtParametro);
        buttonEnviar = (Button) findViewById(R.id.buttonEnviarParametro);
        buttonEnviar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                Intent intent  = new Intent(
                        ActividadPasarParametro.this, ActividadRecibirParametro.class);
                Bundle bundle = new Bundle();
                bundle.putString("dato",cajaDatos.getText().toString());
                intent.putExtras(bundle);
                startActivity(intent);
            }
        });

    }
}